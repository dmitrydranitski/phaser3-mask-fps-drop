import 'phaser';
import UIPlugin from 'Rex/rexuiplugin.min.js';

import {SCREEN_WIDTH, SCREEN_HEIGHT} from 'Services/responsiveLayoutCalculator.js';
import PlayScene from 'Scenes/PlayScene.js';

const config = {
  type: Phaser.AUTO,
  scale: {
    mode: Phaser.Scale.NONE, // we will resize the game with our own code (see Boot.js)
    width: SCREEN_WIDTH, // set game width by multiplying canvas width with devicePixelRatio
    height: SCREEN_HEIGHT, // set game height by multiplying canvas height with devicePixelRatio
    zoom: 1/window.devicePixelRatio,
  },
  scene: PlayScene,
  plugins: {
    scene: [{
      key: 'rexUI',
      plugin: UIPlugin,
      mapping: 'rexUI',
    }],
  },
};

const game = new Phaser.Game(config);
