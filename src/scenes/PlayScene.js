import PowersBar from '../components/PowersBar';
import {POWER_START} from '../services/constants';
import between from 'phaser/src/math/Between';
import {SCREEN_WIDTH, SCREEN_HEIGHT} from '../services/responsiveLayoutCalculator';

export default class PlayScene extends Phaser.Scene {
  constructor() {
    super({
      key: 'PlayScene',
    });
  }

  preload() {
    for (let p = 0; p <= 3; p++) {
      this.load.image('POWER'+p, `${p}.png`);
    }

    this.load.spritesheet('balls', 'https://labs.phaser.io/assets/sprites/balls.png', {frameWidth: 17, frameHeight: 17});
  }

  balls() {
    this.i++;

    const bob = this.blitter.create(this.x, SCREEN_HEIGHT*0.9, Phaser.Math.Between(0, 5));

    this.x += 2;

    if (this.x >= SCREEN_WIDTH) {
      this.x = 0;
    }

    this.text.setText('Active Tweens: ' + this.tweens._active.length + '\nTotal Tweens created: ' + this.i);

    this.tweens.add({
      targets: bob,
      y: 10,
      duration: Phaser.Math.Between(500, 1000),
      ease: 'Power1',
      yoyo: true,
      onComplete: function() {
        bob.destroy();
      },
    });
  }

  create() {
    // balls
    this.i = 0;
    this.x = 0;
    this.blitter = this.add.blitter(0, 0, 'balls');
    this.text = this.add.text(SCREEN_WIDTH/2, SCREEN_HEIGHT*0.9);
    this.time.addEvent({delay: 10, callback: this.balls, callbackScope: this, repeat: 10000});
    // balls

    // -------- init powers in registry
    for (let p = 0; p <= 3; p++) {
      this.registry.set('POWER'+p, POWER_START);
    }

    setInterval(() => {
      const power = between(0, 3);
      this.registry.set('POWER'+power, this.registry.get('POWER'+power)+between(-1, 1));
    }, 2000);

    this.powersBar = new PowersBar(this); // horizontal bar in top of screen that contains 4 powers
    this.powersBar.layout();
    this.powersBar.setMaskPosition();
  }
}
