import {POWER_OFFSET, POWER_SIDE, SCALE, SCREEN_WIDTH, SCREEN_HEIGHT} from '../services/responsiveLayoutCalculator';
import {POWER_BACK_COLOR, POWER_COLOR, BORDER_COLOR, POWER_AFFECT_POSITIVE_COLOR, POWER_AFFECT_NEGATIVE_COLOR} from '../services/colors';
import {POWER_MAX} from '../services/constants';

export default class PowersBar {
  constructor(scene) {
    // horizontal bar in top of screen that contains 4 powers
    const powersBar = scene.rexUI.add.sizer({
      x: SCREEN_WIDTH/2,
      y: SCREEN_HEIGHT/2,
      orientation: 'x',
      width: (POWER_OFFSET) * 4,
      height: POWER_SIDE,
    })
        .setDepth(2)
        .addBackground(scene.rexUI.add.roundRectangle(0, 0, 10*SCALE, 10*SCALE, 10*SCALE, BORDER_COLOR.int).setDepth(2));

    this.powers = [];
    for (let p = 0; p <= 3; p++) {
      // mask
      const power_mask = scene.make.sprite({
        key: 'POWER'+p,
        add: false,
      }).setDisplaySize(POWER_SIDE * 0.85, POWER_SIDE * 0.85);

      // power level background
      const power_level_back = scene.add.sprite(0, 0, 'POWER'+p).setDisplaySize(POWER_SIDE * 0.85, POWER_SIDE * 0.85).setTint(POWER_BACK_COLOR.int);

      // power level
      const power_level = scene.rexUI.add.roundRectangle(0, 0, POWER_SIDE, POWER_SIDE, 0, POWER_COLOR.int);
      power_level.mask = new Phaser.Display.Masks.BitmapMask(scene, power_mask); // <--- COMMENT

      const power_container = scene.add.container().setDepth(3)
          .add([power_level_back, power_level]);

      // set valid mask position after sizers layout() - when power container will change position
      power_container.setMaskPosition = () => {
        power_mask.setPosition(power_container.x, power_container.y);
      };

      scene.registry.events.on('changedata-POWER'+p, (parent, value, previous) => {
        console.log(`POWER${p} changed to ${value}, current y:${power_level.y}, will make y:${POWER_SIDE * (1 - value/POWER_MAX)} same as P*(${(1 - value/POWER_MAX)}), POWER_SIDE=`+POWER_SIDE);
        if (value < 0) value = 0;
        if (value > POWER_MAX) value = POWER_MAX;
        scene.tweens.add({
          targets: power_level,
          y: POWER_SIDE * (1 - value/POWER_MAX),
          duration: 400,
        });

        if (previous != value) {
          const c1 = POWER_COLOR.color;
          const c2 = value > previous ? POWER_AFFECT_POSITIVE_COLOR.color : POWER_AFFECT_NEGATIVE_COLOR.color;

          power_level.tweenStep = 0;
          scene.tweens.add({
            targets: power_level,
            tweenStep: 100,
            onUpdate: ()=>{
              const col = Phaser.Display.Color.Interpolate.ColorWithColor(c1, c2, 100, power_level.tweenStep);
              const colourInt = Phaser.Display.Color.GetColor(col.r, col.g, col.b);
              power_level.setFillStyle(colourInt);
            },
            duration: 400,
            yoyo: true, // Return to first tint
          });
        }
      });

      // set initial power level
      power_level.y = POWER_SIDE * (1 - scene.registry.get('POWER'+p)/POWER_MAX);

      this.powers.push(power_container);
      powersBar.add(power_container, 0, 'center', {left: POWER_OFFSET, top: 3 * SCALE, bottom: 3 * SCALE, right: POWER_OFFSET}, false);
    }

    powersBar.setMaskPosition = () => {
      this.powers[0].setMaskPosition();
      this.powers[1].setMaskPosition();
      this.powers[2].setMaskPosition();
      this.powers[3].setMaskPosition();
    };

    return powersBar;
  }
}
