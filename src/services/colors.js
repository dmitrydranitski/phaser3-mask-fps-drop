const _colors = {
  'power_color': '#f6f3bc',
  'power_affect_positive_color': '#78fa61',
  'power_affect_negative_color': '#fa6161',
  'power_background_color': '#8b7a4e',
  'border_color': '#231401',
  'main_background_color': '#bda867',
  'card_background': '#10220e',
  'symbol_color': '#7d6648',
  'post_text_card_color': '#e9e8e4',
  'post_text_card_symbol_color': '#e1dad3',
  'answer_text_color': '#ffffff',
};

export const POWER_COLOR = getColorObject(_colors.power_color);
export const POWER_AFFECT_POSITIVE_COLOR = getColorObject(_colors.power_affect_positive_color);
export const POWER_AFFECT_NEGATIVE_COLOR = getColorObject(_colors.power_affect_negative_color);
export const POWER_BACK_COLOR = getColorObject(_colors.power_background_color);

export const BORDER_COLOR = getColorObject(_colors.border_color);
export const MAIN_BACK_COLOR = getColorObject(_colors.main_background_color);

export const CARD_BACK = getColorObject(_colors.card_background);
export const SYMBOL_COLOR = getColorObject(_colors.symbol_color);

export const POST_TEXT_CARD_COLOR = getColorObject(_colors.post_text_card_color);
export const POST_TEXT_CARD_SYMBOL_COLOR = getColorObject(_colors.post_text_card_symbol_color);

export const ANSWER_TEXT_COLOR = getColorObject(_colors.answer_text_color);

function getColorObject(hex) {
  return {
    hex,
    int: Phaser.Display.Color.HexStringToColor(hex).color,
    color: Phaser.Display.Color.ValueToColor(hex),
  };
}
